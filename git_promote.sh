#!/usr/bin/env bash
set -e

APP_VERSION=$(cat version.txt)

git config user.email "fayala.vargas@gmail.com"
git config user.name "GoCD Builder"

git add version.txt
git commit -m "Promoting APP version: ${APP_VERSION} to Dev."
git push
