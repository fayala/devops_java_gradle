pipeline {
    agent {
        docker {
            image 'registry.jala.info/devops/openjdk:11.0.8-jdk'
        }
    }

    triggers {
        pollSCM('H/2 * * * *')
    }

    environment {
        DB_SERVER = 'sqldev.windows.azure.com'
        DB_USER = 'sa'
        DB_PASSWORD = credentials('db_password')
    }

    stages {
        stage('Build') {
            steps {
                echo 'Building..'
                sh './gradlew clean assemble'
            }
            post {
                always {
                    archiveArtifacts artifacts: 'app/build/libs/*.jar'
                }
            }
        }
        stage('Run Tests') {
            parallel {
                stage('Tests on Windows') {
                    steps {
                        echo 'Testing..'
                        sh './gradlew test'
                    }
                    post {
                        always {
                            junit 'app/build/test-results/test/TEST-*.xml'
                            archiveArtifacts artifacts: 'app/build/reports/tests/test/**/*'
                        }
                    }
                }
                stage('Tests on Linux') {
                    steps {
                        echo 'Testing..'
                        sh './gradlew test'
                    }
                    post {
                        always {
                            junit 'app/build/test-results/test/TEST-*.xml'
                            archiveArtifacts artifacts: 'app/build/reports/tests/test/**/*'
                        }
                    }
                }
            }
        }
        stage('Run') {
            steps {
                echo 'Running....'
                echo "Database Server is ${DB_SERVER}"
                sh './gradlew run'
            }
        }
    }
}
